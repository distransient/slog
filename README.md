# Slog

Structured logger for [ysh/Oil][ysh]. 

Copy the file `slog.ysh` somewhere and then use the `source` command to gain access to its functions.

When ysh has full typed functions support this library will change to use those instead of procs.

[ysh]: https://www.oilshell.org/